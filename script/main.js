const request = require('request');

var admin = require("firebase-admin");

var serviceAccount = require("../server/firebase_key.json");

const api_key = "2349ce5e6a91e2352005ecfed1fdf52c"

let CPT = 50;
let ACTORS_CPT = 0;
console.log("Initializing...");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://dlc-esir3.firebaseio.com"
});
console.log("Initialized.");

function main() {
  setInterval(getMoviesFromPage, 11000, 300);
}

const newLocal = "https://api.themoviedb.org/3/discover/movie?api_key="+api_key+"&language=fr-FR&sort_by=popularity.desc&include_adult=false&include_video=false&release_date.lte=2019-01-01&page=";
function getMoviesFromPage(limit) {
  request(newLocal+CPT,{json: true}, (err, res, body) => {
    if (err) { return console.log(err); }
    if (body.results) {
      console.log("Writing " + body.results.length + " movies..." );
      addResultsToDB(body.results);
      console.log("Scraped page " + CPT);
      if( CPT < limit) {
          CPT++;
          getMoviesFromPage(CPT, limit);
      } else {
          console.log("Finished.");
          return 0;
      }
    }
  })

}

function getMovieInfo(id, callback) {
  request("https://api.themoviedb.org/3/movie/"+id+"?api_key="+api_key+"&language=fr-FR",{json: true}, (err, res, body) => {
    if (err) { return console.log(err); }
    callback(body)
  });
}


function addResultsToDB(results) {
  for (var i = 0; i < results.length; i++) {
    console.log("RESULT " + results[i]);
    getMovieInfo(results[i].id, writeMovie)
  }
}

function writeMovie(movie) {
  if (typeof movie !== 'undefined' && typeof movie.title !== 'undefined' ) {
    console.log("Writing " + movie.title);
    return admin.database().ref('movies/' + movie.id).set(movie);
  } else {
    console.log(movie);
  }
}

function fillActors() {
  var movies = admin.database().ref('movies')
  .on('value', function(snapshot) {
    const data = snapshot.val() || null;
    if (data) {
      const ids = Object.keys(data);
      var i,j,temparray,chunk = 40;
      var splittedIds = []; // array of 40 items arrays
      for (i=0,j=ids.length; i<j; i+=chunk) {
          temparray = ids.slice(i,i+chunk);
          splittedIds.push(temparray)
      }
      var i = 0
      setInterval(extractActors, 11000, splittedIds)
    }
    
  })
}

function extractActors(splittedIds) {
  if (ACTORS_CPT < splittedIds.length) {
    splittedIds[ACTORS_CPT].forEach(id => {
      getActorsForMovie(id, (res) => {
        writeActor(res.cast, res.id)
        
      })
    });
    ACTORS_CPT++
  } else {
    console.log("done for actors.");
    return 0
  }
  
}

function writeActor(actors, movieId) {
  actors.forEach(actor => {
    console.log("Writing " + actor.name + " for movie " + movieId);
    return admin.database().ref('actors/' + movieId + '/' + actor.id).set(actor);
  })
}

function getActorsForMovie(movieId, callback) {
  request("https://api.themoviedb.org/3/movie/"+movieId+"/credits?api_key="+api_key,{json: true}, (err, res, body) => {
    if (err) { return console.log(err); }
    callback(body)
  });
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitTenSeconds() {
  console.log('Taking a break...');
  await sleep(10000);
  console.log('10 seconds later');
}


// main();
//admin.database().ref('actors').remove()
fillActors();

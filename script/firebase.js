var admin = require("firebase-admin");

var serviceAccount = require("../server/firebase_key.json");

var count = 0;
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://dlc-esir3.firebaseio.com"
});

var db = admin.database();
var ref = db.ref("movies");
ref.on("child_added", function(snapshot, prevChildKey) {
  var newPost = snapshot.val();
  // console.log("Genres: " + newPost.genres);
  console.log("Title: " + newPost.title);
  // console.log("Previous Post ID: " + prevChildKey);
  count ++;
  console.log("Inserted " + count + " items.")
});
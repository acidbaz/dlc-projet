const express = require('express');
var firebase  = require('firebase');
var path = require('path');
const functions = require('firebase-functions');
const bodyParser = require("body-parser");
const app = express();

//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var config = {
  apiKey: "AIzaSyA7v2uAqPIpDew_dDh83jnGilEdtEjt2xU",
  authDomain: "dlc-esir3.firebaseapp.com",
  databaseURL: "https://dlc-esir3.firebaseio.com",
  projectId: "dlc-esir3",
  storageBucket: "dlc-esir3.appspot.com",
  messagingSenderId: "133442809772"
};
firebase.initializeApp(config);

var db = firebase.database();
var ref = db.ref("/movies");

app.get('/', (request, response) => {
	response.sendFile(path.resolve('public/index.html'));
});

app.get('/all', (request, response) => {
  ref.orderByChild("popularity").once("value", function(snapshot) {
    var data = [];
    snapshot.forEach((value) => {
      data.unshift(value.val())
    });
    response.send(data);
  });
});

app.get('/movies', (request, response) => {
  var from = Number(request.query.from);
  var to = Number(request.query.to);

  if(isNaN(from)||isNaN(to)){
    response.status(400).send("Problem encoutred");
    return;
  }
  if (from>to){
    response.status(400).send("Parameters out of range");
    return;
  }
  var lastKnownKey = null;
  var firstQuery = ref.orderByKey().limitToFirst(from);
  firstQuery.once('value', function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
      lastKnownKey = childSnapshot.key;
    });
    
    if(lastKnownKey){
      ref.orderByKey().startAt(lastKnownKey).limitToFirst(to-from+1).once('value',function(snapshot){
        var data = [];
        snapshot.forEach((value) => {
          data.push(value.val())
        });
        response.send(data);
      });
    }else{
  		response.status(400).send("Problem encoutred");
  	}
  });
});

app.get('/movies_by_date', (request, response) => {
  var from = Number(request.query.from);
  var to = Number(request.query.to);
  if(isNaN(from)||isNaN(to)){
    response.status(400).send("Problem encoutred");
    return;
  }
  if (from>to){
    response.status(400).send("Parameters out of range");
    return;
  }
  var lastKnownKey = null;
  var firstQuery = ref.orderByChild("release_date").limitToLast(from);
  firstQuery.once('value', function(snapshot) {
    var maxval;
    snapshot.forEach(function(childSnapshot) {
      if(!maxval){
       maxval = childSnapshot.child("release_date").val();
      }
    });
    console.log(maxval)
    if(maxval){
      ref.orderByChild("release_date").limitToLast(to).once('value',function(snapshot){
        var data = [];
        var i=0;
        snapshot.forEach((value) => {
          if (i<to-from+1){
            data.unshift(value.val());
            i++;
          }
        });
        response.send(data);
      });
    }else{
      response.status(400).send("Problem encoutred");
    }
  });
});

app.get('/movies_by_popularity', (request, response) => {
  var from = Number(request.query.from);
  var to = Number(request.query.to);
   if(isNaN(from)||isNaN(to)){
    response.status(400).send("Problem encoutred");
    return;
  }
  if (from>to){
  	response.status(400).send("Parameters out of range");
    return;
  }
  var lastKnownKey = null;
  var firstQuery = ref.orderByChild("popularity").limitToLast(from);
  firstQuery.once('value', function(snapshot) {
    var maxval;
  	snapshot.forEach(function(childSnapshot) {
      if(!maxval){
    	 maxval = childSnapshot.child("popularity").val();
      }
  	});
    console.log(maxval)
  	if(maxval){
  		ref.orderByChild("popularity").limitToLast(to).once('value',function(snapshot){
        var data = [];
        var i=0;
        snapshot.forEach((value) => {
          if (i<to-from+1){
            data.unshift(value.val());
            i++;
          }
        });
  			response.send(data);
  		});
    }else{
  		response.status(400).send("Problem encoutred");
  	}
  });
});

app.get('/pages',(request,response)=>{
  ref.once("value", function(snapshot) {
    var count = snapshot.numChildren();
    response.status(200).send(Math.ceil(count/10).toString());
    
  });
});


app.get('/count',(request,response)=>{
  ref.once("value", function(snapshot) {
    var count = snapshot.numChildren();
    response.status(200).send(count.toString());
    
  });
});

app.get('/get', (request, response) => {
  const movieId = request.query.id;
  ref.child(movieId).once('value', function(movie) {
    response.send(movie);
  });
});

// search movies
app.get('/search', (request, response) => {
  var string = request.query.string;
  console.log("Searching for :"+string);
  ref.orderByChild("title").startAt(string).endAt(string+"\uf8ff").once('value',function(snapshot){
    if(snapshot.val()==null){
      response.sendFile(path.resolve('public/404.html'));
    }else{
      var data = [];
      snapshot.forEach((value) => {
        data.push(value.val())
      });
      response.send(data);
    }
  });
});

app.get('/actors', (request, response) => {
  const movieId = request.query.movieId
  const actorsRef = db.ref('actors/' + movieId)
  actorsRef.once('value', (snapshot) => {
    response.send(snapshot.val())
  })
})

app.post('/add', (request, response) => {
  var name = request.body.name;
  var date = request.body.date;
  var overview = request.body.overview;
  var budget = request.body.budget;
  var popularity = request.body.popularity;
  
  var id = ref.orderByKey().limitToLast(1);
  id.once('value', function(snapshot) {
    var id = Object.keys(snapshot.val())[0];
    if(id<1000000){
      id=1000000;
    }
    id++;

    ref.child(String(id)).set({ 
      id: id,
      title: name,
      release_date: date,
      overview: overview,
      budget: budget,
      popularity: popularity
    });
    
    console.log("Film succesfully added with ID : "+id);
    response.status(200).send("Film succesfully added with ID : "+id);    
    
   
  });
});

app.use(function(request, response){
	response.sendFile(path.resolve('public/404.html'));
});

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.app = functions.https.onRequest(app);

/* server = app.listen(3000, function () {
  console.log("app running on port.", server.address().port);
  
});*/
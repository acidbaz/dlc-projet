const express = require('express');

const request = require('request')

const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));

 const backendUrl = "https://dlc-esir3.firebaseapp.com";
 //const backendUrl = "http://localhost:3000"

const MOVIES_PER_PAGE = 10;
const PORT = 8081;

let all_movies = [];

app.use(express.static("public"))
app.set('view engine', 'pug');
app.get('/', (req, res) => {
    request(backendUrl + "/movies_by_popularity?from=1&to="+MOVIES_PER_PAGE, (error, response, body) => {
        try {
            const json = JSON.parse(body)
        all_movies = json
        res.render('home', {movies: all_movies, moviesNumber: all_movies.length, title: 'Tous les films', active: 'all', page: 1}); 
        } catch (e) {
            console.log("Wrong json ", body);
            res.render('500')
        }       
    })
});

app.get('/all', (req, res) => {
    const page = req.query.page;
    if (isNaN(page)){
        res.render('500');
        return;
    }
    var from = 1
    if (page > 1) {
        from = ((page-1)*MOVIES_PER_PAGE)+1
    }
    request(backendUrl + "/movies_by_popularity?from="+from + "&to=" + page*MOVIES_PER_PAGE, (error, response, body) => {
       try{
        const json = JSON.parse(body)  
        all_movies = json
        res.render('home', {movies: all_movies, moviesNumber: all_movies.length, title: 'Tous les films', active: 'all', page: page});
        } catch (e) {
            console.log("Wrong json ", body);
            res.render('500')
        }       
    })
});

app.get('/latest', (req, res) => {
    const page = req.query.page;
    var from = 1
    if (page > 1) {
        from = ((page-1)*MOVIES_PER_PAGE)+1
    }
    request(backendUrl + "/movies_by_date?from="+from + "&to=" + page*MOVIES_PER_PAGE, (error, response, body) => {
       try{
        const json = JSON.parse(body)  
        all_movies = json
        res.render('home', {movies: all_movies, moviesNumber: all_movies.length, title: 'Films récents', active: 'latest', page: page});
        } catch (e) {
            console.log("Wrong json ", body);
            res.render('500')
        }       
    })
});

app.get('/search', (req, res) => {
    const search = req.query.searchQuery;
    request(backendUrl + '/search?string='+search, (error, response, body) => {
        if (error) throw error        
        res.render('home', {movies: JSON.parse(body), title: 'Résultat de la recherche', active: 'search'})
    }) 
});

app.get('/details', (req, res) => {
    const movieId = req.query.movieId
    const page = req.query.page
    request(backendUrl + "/get?id=" + movieId, (err, response, body) => {
        try {
            const movie = JSON.parse(body)
            res.render('movie_details', {movie: movie, page: page})
        } catch (e) {
            console.log("Error getting movie details for id ", movieId)
            res.render('500')
        }
    })
})

app.get('/add', (req, res) => {
    res.render('add_movie');
});

app.post('/addMovie', (req, res) => {
    const formData = {
        name: req.body.original_title,
        poster: req.body.poster,
        date: req.body.release_date,
        overview: req.body.overview,
        budget: Number(req.body.budget),
        popularity: Number(req.body.popularity)
    }
    console.log(formData);
    
    request.post(backendUrl + '/add', {
        json: formData
    },
    (error, response, body) => {
        if (error) {
            throw error;
        }
        res.render('add_movie');
    });
});

app.get('/about', (req, res) => {
    request(backendUrl  + '/count', (error, response, body ) => {
        try {
            res.render('about', {count: body})
        } catch (e) {
            console.error("Error getting count.", e);
            res.render('500')
        }
    })
})

app.get('/actors', (req, res) => {
    request(backendUrl + '/actors?movieId=' + req.query.movieId, (e, r, b) => {
        request(backendUrl + "/get?id=" + req.query.movieId, (err, response, body) => {
            if ( b.length > 0 ) {
                try {
                    res.render('actors', {movie: JSON.parse(body), actors: JSON.parse(b)})
                } catch (exception) {
                    console.error("Error getting actors.", exception);
                    res.render('500')
                }
            } else {
                res.render('actors', {movie: JSON.parse(body), actors: []})
            }
            
        })
    })
})

app.get('*', (req, res) => {
    res.render('404')
});


let server = app.listen(PORT, function () {
  console.log("app running on port.", server.address().port);
  
});
//exports.app = functions.https.onRequest(app);

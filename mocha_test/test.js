var assert = require('assert');
var global = require('./global.js');

var chai = require('chai')
  , chaiHttp = require('chai-http');

let should = chai.should();
let expect = chai.expect

chai.use(chaiHttp);



describe('get movies from 1 to 5 length', () => {	
	it('it should get a json of 5 elements', function(done) {
		this.timeout(30000);
		chai.request('https://dlc-esir3.firebaseapp.com')
		.get('/movies?from=1&to=5')
		.end((err, res) => {
		res.body.length.should.be.eql(5);
		
	done();
	});
});
});

describe('get movies from 20 to 20 length', () => {	
	it('it should get a json of 1 element', function(done) {
		this.timeout(30000);
		chai.request('https://dlc-esir3.firebaseapp.com')
		.get('/movies?from=20&to=20')
		.end((err, res) => {
		res.body.length.should.be.eql(1);
		
	done();
	});
});
});

describe('check fields of all elements', () => {	
	it('it checks if certains fields are present (id, popularity...) for all elements', function(done) {
		this.timeout(30000);
		chai.request('https://dlc-esir3.firebaseapp.com')
		.get('/all')
		.end((err, res) => {
		//res.body.forEach(function(element) {
    		//element.should.have.property('adult');
		//element.should.have.property('budget');
    		element.should.have.property('id');
    		//element.should.have.property('original_language');
    		element.should.have.property('original_title');
    		//element.should.have.property('overview');
    		element.should.have.property('popularity');
    		//element.should.have.property('revenue');
    		//element.should.have.property('status');
    		//element.should.have.property('tagline');
    		element.should.have.property('title');
    		//element.should.have.property('video');
    		//element.should.have.property('vote_average');
    		//element.should.have.property('vote_count');

});
		
		
	done();
	});
});

describe('check if the popularity rank is well', () => { 
	it('it checks if the popularity of element n is higher than the popularity of n+1', function(done){
		this.timeout(30000);
		chai.request('https://dlc-esir3.firebaseapp.com')
		.get('/movies_by_popularity?from=1&to=5')
		.end((err, res) => {
		//l<et array = [...res.body]; //not iterable
		let array = Array.from(res.body);
		console.log(array);
		var popularity;
		popularity = 536987;
		array.forEach(function(element) {
	
		popularity.should.least(element.popularity);
		popularity = element.popularity;
    		

});
		
		
	done();
	});
});
});

/*describe('check if a movie is present twice', () => {
	//Fondamentally a wrong test, because there may be both the same title : King Kong for example
	it('parse all the movies in /all an check that two movies have not the same title', function(done) {
		this.timeout(30000);
		chai.request('https://dlc-esir3.firebaseapp.com')
		.get('/all')
		.end((err, res) => {
		let ArrayTittle;
		ArrayTittle = [];
		let ArrayTemp;
		
		res.body.forEach(function(element) {
		
		ArrayTemp = [];
		ArrayTemp.push(element.title);
		//remove comment of next line to have a veridique test
		//expect(ArrayTittle).to.not.include.members(ArrayTemp);
		ArrayTittle.push(element.title);


});
		
		
	done();
	});
});
});*/

/*
describe('check if all elements of a research contain the searched expression', () => {
	it('parse all the movies in the research result an check if the title of the movie contain the expression', function(done) {
		this.timeout(60000);
		
		global.ExpressionToSearch.forEach(function(Expression) {
		chai.request('127.0.0.1:3000')
		.get('/search?string=' + Expression)
		.end((err, res) => {
		
		let array = Array.from(res.body);
		array.forEach(function(element) {
	
		expect(element.title).to.include(Expression);



});
		
	done();
	return;
	});
});	
});	
});
*/





